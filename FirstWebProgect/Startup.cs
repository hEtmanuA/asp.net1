﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FirstWebProgect.Startup))]
namespace FirstWebProgect
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
