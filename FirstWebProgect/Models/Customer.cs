﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FirstWebProgect.Models
{
    public class Customer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
}