﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstWebProgect.Models;

namespace FirstWebProgect.Controllers
{
    public class HomeController : Controller
    {
        public static ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var cust = db.Customers;
            return View(cust);
        }

        [HttpPost]
        public ActionResult Index(Customer customer)
        {
            db.Customers.Add(customer);
            db.SaveChanges();
            return View("Index", db.Customers.ToList());
        }
        public ActionResult Delete(int id)
        {
            Customer inst = db.Customers.Find(id);
            db.Customers.Remove(inst);
            db.SaveChanges();
            return View("Index",db.Customers.ToList());
        }
        
        public ActionResult Save(Customer newModel)
        {
            
            Customer oldModel = db.Customers.Find(newModel.id);
            oldModel.title = newModel.title;
            oldModel.name = newModel.name;
            oldModel.description = newModel.description;
            db.SaveChanges();
            return View("Index",db.Customers.ToList());
        }
    }
}